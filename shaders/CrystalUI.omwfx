uniform_vec2 uBlurBounds {
    default = vec2(0, 10000000);
    static = false;
}

shared {
    vec4 blur(vec2 direction, vec2 uv) {
        vec4 color = vec4(0.0);
        vec2 off1 = vec2(1.3846153846) * direction;
        vec2 off2 = vec2(3.2307692308) * direction;
        color += omw_GetLastPass(uv) * 0.2270270270;
        color += omw_GetLastPass(uv + (off1 / omw.resolution)) * 0.3162162162;
        color += omw_GetLastPass(uv - (off1 / omw.resolution)) * 0.3162162162;
        color += omw_GetLastPass(uv + (off2 / omw.resolution)) * 0.0702702703;
        color += omw_GetLastPass(uv - (off2 / omw.resolution)) * 0.0702702703;
        return color;
    }
}

fragment blurV {
    omw_In vec2 omw_TexCoord;

    void main()
    {
        omw_FragColor = blur(vec2(0, 1), omw_TexCoord);
    }
}

fragment blurH {
    omw_In vec2 omw_TexCoord;

    void main()
    {
        omw_FragColor = blur(vec2(1, 0), omw_TexCoord);
    }
}


fragment main {

    omw_In vec2 omw_TexCoord;

    void main()
    {
        vec4 origScene = omw_GetLastPass(omw_TexCoord);
        vec2 uv = omw_TexCoord;
        uv.y = 1.0 - uv.y;
        vec2 texel = omw.resolution * uv;
        float softness = 50;
        float softEdge = smoothstep(uBlurBounds.x - softness, uBlurBounds.x, texel.y);
        softEdge *= 1.0 - smoothstep(uBlurBounds.y - softness, uBlurBounds.y, texel.y);
        vec4 mask = mix(vec4(0.0, 0.0, 0.0, 0.0), origScene, softEdge);

        omw_FragColor = mix(origScene, mask, 0.68);
    }
}

technique {
    passes = blurV, blurH, main;
    author = "Wazabear";
    version = "1.0";
    dynamic = true;
}
