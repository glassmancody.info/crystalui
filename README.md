# CrystalUI

A re-imagining of the Morrowind UI. This will eventually be a replacement for all views. As the Lua API progresses more functionality will be added.

## TODO

- [ ] Spell window
- [ ] Barter window
- [ ] Inventory window
- [ ] Companion window
- [ ] Alchemy window
- [ ] Map window
- [ ] Spell window
- [ ] Spell creation window
- [ ] Stats window
- [ ] Book view
- [ ] Journals view
- [ ] Scroll view
- [ ] Configurable HUD
- [ ] QuickLoot
- [ ] [UIModes](https://gitlab.com/ptmikheev/openmw-lua-examples/-/tree/master/UiModes) Integration

## Note

This is a heavy WIP mod, **not ready** for public consumption. It's only on GitLab for my own sake. When this is released you will know :)

When the core functionality is added I'll open up a testing phase when interested.
