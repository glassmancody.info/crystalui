local ambient = require('openmw.ambient')

local function playClick()
    if ambient.isSoundPlaying("Menu Click") then return end

    ambient.playSound("Menu Click", {
        volume = 1.0,
    })
end

return {
    playClick = playClick,
}
