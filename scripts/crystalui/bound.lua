local core = require('openmw.core')

return {
    [string.lower(core.getGMST('sMagicBoundDaggerID'))] = true,
    [string.lower(core.getGMST('sMagicBoundLongswordID'))] = true,
    [string.lower(core.getGMST('sMagicBoundMaceID'))] = true,
    [string.lower(core.getGMST('sMagicBoundBattleAxeID'))] = true,
    [string.lower(core.getGMST('sMagicBoundSpearID'))] = true,
    [string.lower(core.getGMST('sMagicBoundLongbowID'))] = true,
    [string.lower(core.getGMST('sMagicBoundCuirassID'))] = true,
    [string.lower(core.getGMST('sMagicBoundHelmID'))] = true,
    [string.lower(core.getGMST('sMagicBoundBootsID'))] = true,
    [string.lower(core.getGMST('sMagicBoundShieldID'))] = true,
    [string.lower(core.getGMST('sMagicBoundLeftGauntletID'))] = true,
    [string.lower(core.getGMST('sMagicBoundRightGauntletID'))] = true,
}
