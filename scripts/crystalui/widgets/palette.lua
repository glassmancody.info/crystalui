local util = require('openmw.util')

return {
    Text_Normal = util.color.rgb(202 / 255, 165 / 255, 96 / 255),
    Text_Normal_Over = util.color.rgb(223 / 255, 201 / 255, 159 / 255),
    Text_Normal_Pressed = util.color.rgb(243 / 255, 237 / 255, 221 / 255),
    Text_Active = util.color.rgb(96 / 255, 112 / 255, 202 / 255),
    Text_Active_Over = util.color.rgb(159 / 255, 169 / 255, 223 / 255),
    Text_Active_Pressed = util.color.rgb(223 / 255, 226 / 255, 244 / 255),
    Text_Disabled = util.color.rgb(179 / 255, 168 / 255, 135 / 255),
    Text_Disabled_Over = util.color.rgb(223 / 255, 201 / 255, 159 / 255),
    Text_Disabled_Pressed = util.color.rgb(243 / 255, 237 / 255, 221 / 255),
}
