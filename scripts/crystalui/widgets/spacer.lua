local function Spacer()
    return {
        external = {
            grow = 1,
        }
    }
end

return Spacer
