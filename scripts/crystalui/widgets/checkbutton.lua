local ui = require('openmw.ui')
local vec2 = require('openmw.util').vector2
local util = require('openmw.util')
local I = require('openmw.interfaces')
local async = require('openmw.async')

local Palette = require('scripts.crystalui.widgets.palette')
local Sounds = require('scripts.crystalui.sounds')

local function toggle(self, enabled)
    if (enabled) then
        self.content[1].content[1].props.textColor = Palette.Text_Active_Pressed
    else
        self.content[1].content[1].props.textColor = Palette.Text_Normal
    end
end

local function CheckButton(options)
    return {
        _toggle = toggle,
        type = ui.TYPE.Container,
        name = options.id,
        userData = {
            checked = options.checked and true or false,
            label = options.label,
            id = options.id,
        },
        events = {
            mousePress = async:callback(function(e, data)
                if (e.button ~= 1 or options.onChange == nil) then return end
                data.userData.checked = not data.userData.checked

                if (data.userData.checked) then
                    data.content[1].content[1].props.textColor = Palette.Text_Active_Pressed
                else
                    data.content[1].content[1].props.textColor = Palette.Text_Normal
                end

                Sounds.playClick()
                options.onChange(data)

                return true
            end),
        },
        content = ui.content {
            {
                template = I.MWUI.templates.box,
                content = ui.content {
                    {
                        template = I.MWUI.templates.textNormal,
                        props = {
                            textAlignH = ui.ALIGNMENT.Center,
                            textAlignV = ui.ALIGNMENT.Center,
                            text = options.label,
                            textColor = options.checked and Palette.Text_Active_Pressed or Palette.Text_Normal,
                            readOnly = true,
                            textSize = 16,
                            autoSize = false,
                            inheritAlpha = false,
                            size = vec2(math.max(85, #options.label * 12), 25),
                        },
                    },
                },
            },
        },
    }
end

return CheckButton
