local vec2 = require('openmw.util').vector2

local function Padding(size)
    return {
        props = {
            size = vec2(size or 0, size or 0),
        }
    }
end

return Padding
