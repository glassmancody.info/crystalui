local vec2 = require('openmw.util').vector2
local util = require('openmw.util')
local I = require('openmw.interfaces')
local ui = require('openmw.ui')
local async = require('openmw.async')

local Padding = require('scripts.crystalui.widgets.padding')
local Palette = require('scripts.crystalui.widgets.palette')
local ItemViewRowHeader = require('scripts.crystalui.widgets.itemviewrowheader')
local ItemViewRow = require('scripts.crystalui.widgets.itemviewrow')

local state = require('scripts.crystalui.state.state')

local HeightPerRow = 18
local NumRows = 21
local TotalHeight = HeightPerRow * NumRows

local function render(self)
    local columns = self.userData.columns

    self.content['header'] = ItemViewRowHeader({
        columns = columns,
        view = self,
        name = 'header'
    })

    -- calculate the range that will be visible in the scroll view
    local firstVisibleRow = math.floor(state[self.userData.type].itemViewScrollPos / HeightPerRow)
    local lastVisibleRow = firstVisibleRow + NumRows - 1

    self.content['items'].content = ui.content {}

    for i = 1, math.min(NumRows, #self.userData.items) do
        self.content['items'].content:add(ItemViewRow({
            item = self.userData.items[i],
            columns = columns,
            view = self,
            index = i
        }))
    end

    self:_updateSelectedRow()

    -- update scrollbar height
    local totalItemCount = #self.userData.items
    local totalRowHeight = totalItemCount * HeightPerRow

    local scrollBarHeightPercentage = TotalHeight / totalRowHeight
    if scrollBarHeightPercentage >= 1 then scrollBarHeightPercentage = 0 end

    local scrollBarHeight = scrollBarHeightPercentage * TotalHeight

    -- don't allow the scroll bar height to become too small
    if scrollBarHeight > 0 and scrollBarHeight < 35 then scrollBarHeight = 35 end

    self.content['items'].template.content['bar'].content[1].props.size = vec2(12, scrollBarHeight)
end

local function update(self)
    local sorter = state[self.userData.type].selectedSorter
    local ascending = state[self.userData.type].selectedSorterDir

    local column = nil

    -- Iterate through the table to find the item with the matching name
    for _, col in ipairs(self.userData.columns) do
        if col.label == sorter then
            column = col
            break
        end
    end

    if column and column.sortFn then
        self:_sort(ascending, column.sortFn)
    end

    render(self)
end

local function sort(self, ascending, sortFn)
    state[self.userData.type].itemViewScrollPos = 0

    table.sort(self.userData.items, function(a, b)
        -- show equipped items at top
        if a.equipped and b.equipped then
            if ascending then
                return sortFn(a, b)
            else
                return not sortFn(a, b)
            end
        elseif a.equipped then
            return true
        elseif b.equipped then
            return false
        end


        if ascending then
            return sortFn(a, b)
        else
            return not sortFn(a, b)
        end
    end)

    render(self)
end

local function scrollUp(self)
    local row = self.content['items'].content[state[self.userData.type].selectedRow]

    local min = math.max(state[self.userData.type].itemViewScrollPos - TotalHeight, 0)

    if row.userData.index * HeightPerRow > min then return end

    state[self.userData.type].itemViewScrollPos = math.max(state[self.userData.type].itemViewScrollPos - HeightPerRow, 0)

    render(self)
end

local function scrollDown(self)
    local row = self.content['items'].content[state[self.userData.type].selectedRow]

    local max = state[self.userData.type].itemViewScrollPos

    if row.userData.index * HeightPerRow < max then return end

    state[self.userData.type].itemViewScrollPos = state[self.userData.type].itemViewScrollPos + HeightPerRow

    render(self)
end

local function selectNextRow(self)
    local row = self.content['items'].content[state[self.userData.type].selectedRow]
    local index = row.userData.index + 1
    if index > #self.content['items'].content then return end

    state[self.userData.type].selectedRow = index
    state[self.userData.type].selectedItem = self.userData.items[index]

    scrollDown(self)
end

local function selectPreviousRow(self)
    local row = self.content['items'].content[state[self.userData.type].selectedRow]
    local index = row.userData.index - 1
    if index < 1 then return end

    state[self.userData.type].selectedRow = index
    state[self.userData.type].selectedItem = self.userData.items[index]

    scrollUp(self)
end

local ScrollView = {
    type = ui.TYPE.Flex,
    props = {
        relativeSize = vec2(1, 1),
    },
    external = {
        stretch = 1,
    },
    content = ui.content {
        {
            name = 'bar',
            external = {
                stretch = 1,
            },
            props = {
                position = vec2(-15, 0),
                relativePosition = vec2(1.0, 0.0),
                anchor = vec2(1.0, 0.0),
                size = vec2(12, 100),
                relativeSize = vec2(0, 1),
            },
            events = {
                mousePress = async:callback(function(e, self)
                    if self.content[1].userData.focused then return end

                    local oldPos = self.content[1].props.position
                    self.content[1].props.position = vec2(oldPos.x, e.offset.y - (self.content[1].props.size.y / 2))
                    return true
                end),
            },
            content = ui.content {
                {
                    type = ui.TYPE.Image,
                    events = {
                        focusGain = async:callback(function(e, self)
                            self.props.alpha = 0.4
                            self.userData.focused = true
                            return true
                        end),
                        focusLoss = async:callback(function(e, self)
                            self.props.alpha = 1.0
                            self.userData.focused = false
                            return true
                        end),
                        mouseMove = async:callback(function(e, self)
                            -- mouse is not dragging when no button set
                            if not e.button then return true end
                            return true
                        end),
                    },
                    userData = {
                        focused = false,
                    },
                    props = {
                        resource = ui.texture { path = "textures/omw_menu_scroll_center_v.dds" },
                        alpha = 1.0,
                        inheritAlpha = false,
                        size = vec2(12, 0),
                        position = vec2(0, 0),
                    },
                }
            }
        },
        {
            type = ui.TYPE.Flex,
            external = {
                slot = true,
            },
            props = {
                anchor = vec2(0.0, 0.0),
                position = vec2(0, 0),
                size = vec2(-50, 0),
                relativeSize = vec2(1, 1),
            }
        }
    }
}

local function ItemView(options)
    return {
        _sort = sort,
        _update = update,
        _updateSelectedRow = function(self)
            local selected = state[self.userData.type].selectedRow
            -- Ensure a row is selected. If none is then select the first one in the list
            if not pcall(function() self.content['items'].content[selected]:_select() end) then
                if #self.content['items'].content >= 1 then
                    self.content['items'].content[1]:_select()
                end
            end
        end,
        _selectNextRow = selectNextRow,
        _selectPreviousRow = selectPreviousRow,
        type = ui.TYPE.Flex,
        userData = {
            items = {},
            columns = options.columns or {},
            canvasHeight = options.height,
            type = options.type,
        },
        content = ui.content {
            Padding(12),
            {
                name = 'header',
            },
            Padding(12),
            {
                name = 'items',
                template = ScrollView,
                content = ui.content {}
            },
            Padding(12),
        }
    }
end

return ItemView
