local vec2 = require('openmw.util').vector2
local util = require('openmw.util')
local I = require('openmw.interfaces')
local ui = require('openmw.ui')
local async = require('openmw.async')

local Sounds = require('scripts.crystalui.sounds')
local state = require('scripts.crystalui.state.state')

local UpArrow = ui.texture { path = "textures/crystalui/arrow_white.dds" }
local DownArrow = ui.texture { path = "textures/crystalui/arrow_white_down.dds" }

local UpColor = util.color.hex("63cf7a")
local DownColor = util.color.hex("fa7878")

local function ItemViewRowHeader(options)
    local columns = options.columns

    local headers = {}

    for _, v in ipairs(columns) do
        headers[#headers + 1] = {
            type = ui.TYPE.Flex,
            props = {
                horizontal = true,
                size = vec2(v.width, 16),
            },
            userData = {
                view = options.view,
                label = v.label,
                sortFn = v.sortFn
            },
            events = {
                mousePress = async:callback(function(e, data)
                    if not data.userData.sortFn then return end

                    local sorter = state[data.userData.view.userData.type].selectedSorter

                    Sounds.playClick()

                    -- reverse the sorter if it's clicked twice
                    local ascending = true
                    if sorter and data.userData.label == sorter then
                        if state[data.userData.view.userData.type].selectedSorterDir then
                            state[data.userData.view.userData.type].selectedSorterDir = false
                            ascending = false
                        else
                            state[data.userData.view.userData.type].selectedSorterDir = true
                            ascending = true
                        end
                    else
                        -- remove sorter indicator from previous selected sorter
                        if sorter then
                            state[data.userData.view.userData.type].selectedSorterDir = true
                        end

                        state[data.userData.view.userData.type].selectedSorterDir = true
                        ascending = true
                    end

                    state[data.userData.view.userData.type].selectedSorter = data.userData.label
                    data.userData.view:_sort(ascending, data.userData.sortFn)

                    return true
                end),
            },
            content = ui.content {
                {
                    template = I.MWUI.templates.textNormal,
                    props = {
                        textColor = util.color.rgb(0.6, 0.6, 0.6),
                        textAlignH = ui.ALIGNMENT.Start,
                        textAlignV = ui.ALIGNMENT.Center,
                        text = v.label,
                        readOnly = true,
                        textSize = 16,
                        autoSize = true,
                        inheritAlpha = false,
                    },
                },
                {
                    type = ui.TYPE.Widget,
                    props = {
                        size = vec2(20, 16),
                        visible = v.label == state[options.view.userData.type].selectedSorter,
                    },
                    content = ui.content {
                        {
                            type = ui.TYPE.Image,
                            props = {
                                resource = state[options.view.userData.type].selectedSorterDir and
                                    UpArrow or DownArrow,
                                color = state[options.view.userData.type].selectedSorterDir and
                                    UpColor or DownColor,
                                size = vec2(12, 12),
                                alpha = 1.0,
                                position = vec2(2, 2),
                            },
                        }
                    }
                }
            }
        }
    end

    return {
        type = ui.TYPE.Flex,
        name = options.name,
        props = {
            horizontal = true,
        },
        content = ui.content {
            unpack(headers)
        }
    }
end

return ItemViewRowHeader
