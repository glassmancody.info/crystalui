local vec2 = require('openmw.util').vector2
local util = require('openmw.util')
local I = require('openmw.interfaces')
local ui = require('openmw.ui')
local async = require('openmw.async')

local Padding = require('scripts.crystalui.widgets.padding')
local Sounds = require('scripts.crystalui.sounds')

local state = require('scripts.crystalui.state.state')

local function ItemViewRow(options)
    local previewIcon = options.item.icon or "transparent"
    local rowItems = {}

    for _, v in ipairs(options.columns) do
        local icons = {}

        for _, icon in ipairs(v.getIcons and v.getIcons(options.item) or {}) do
            icons[#icons + 1] = Padding(8)
            icons[#icons + 1] = {
                type = ui.TYPE.Image,
                props = {
                    size = vec2(20, 20),
                    resource = ui.texture { path = icon },
                }
            }
        end

        if v.image then
            rowItems[#rowItems + 1] = {
                props = {
                    size = vec2(v.width, v.width),
                },
                content = ui.content {
                    {
                        type = ui.TYPE.Image,
                        props = {
                            inheritAlpha = false,
                            resource = ui.texture { path = previewIcon },
                            size = vec2(24, 24),
                            position = vec2(12, 12)
                        }
                    }
                }
            }
        else
            rowItems[#rowItems + 1] = {
                type = ui.TYPE.Flex,
                props = {
                    horizontal = true,
                    size = vec2(v.width, options.columns[1].width),
                    arrange = ui.ALIGNMENT.Center,
                },
                content = ui.content {
                    {
                        template = I.MWUI.templates.textNormal,
                        props = {
                            textAlignH = ui.ALIGNMENT.Start,
                            textAlignV = ui.ALIGNMENT.Center,
                            text = v.getText and v.getText(options.item) or nil,
                            readOnly = true,
                            textSize = 16,
                            autoSize = true,
                            inheritAlpha = false,
                            relativeSize = vec2(1, 1),
                        },
                    },
                    unpack(icons)
                }
            }
        end
    end

    local background = {
        type = ui.TYPE.Flex,
        props = {
            horizontal = true,
        },
        content = ui.content {
            {
                type = ui.TYPE.Image,
                props = {
                    resource = ui.texture { path = "textures/crystalui/hgradient_white.dds" },
                    relativeSize = vec2(1, 1),
                    position = vec2(0, 0),
                    alpha = 0,
                }
            }
        }
    }

    return {
        _select = function(self)
            local selected = state[self.userData.view.userData.type].selectedRow

            -- unselect row if one is already selected
            if selected then
                pcall(function() self.userData.view.content['items'].content[selected].template.content[1].props.alpha = 0 end)
            end

            self.template.content[1].props.color = util.color.hex('ffffff')
            self.template.content[1].props.alpha = 0.13

            state[self.userData.view.userData.type].selectedRow = self.userData.item.id
            state[self.userData.view.userData.type].selectedItem = self.userData.item
        end,
        template = background,
        events = {
            mousePress = async:callback(function(e, data)
                data:_select()
                Sounds.playClick()

                return true
            end),
            focusLoss = async:callback(function(e, data)
                data.template.content[1].props.color = util.color.hex('ffffff')
                if state[data.userData.view.userData.type].selectedRow == data.userData.item.id then return end
                data.template.content[1].props.alpha = 0.0
                return true
            end),
            focusGain = async:callback(function(e, data)
                state[data.userData.view.userData.type].hoverItem = data.userData.item

                if state[data.userData.view.userData.type].selectedRow == data.userData.item.id then return end

                data.template.content[1].props.color = util.color.hex('BC3535')
                data.template.content[1].props.alpha = 0.1

                return true
            end),
        },
        name = options.item.id,
        userData = {
            view = options.view,
            item = options.item,
            index = options.index,
        },
        content = ui.content {
            unpack(rowItems),
        }
    }
end

return ItemViewRow
