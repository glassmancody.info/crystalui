local ui = require('openmw.ui')
local util = require('openmw.util')
local vec2 = require('openmw.util').vector2

local function Block(options)
    return {
        type = ui.TYPE.Image,
        props = {
            resource = ui.texture { path = "white" },
            color = options.color and util.color.hex(options.color) or util.color.hex("ffffff"),
            relativeSize = vec2(1, 1),
            size = options.size or vec2(8, 8),
        }
    }
end

return Block
