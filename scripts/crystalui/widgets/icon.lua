local ui = require('openmw.ui')
local vec2 = require('openmw.util').vector2
local util = require('openmw.util')
local I = require('openmw.interfaces')

local Padding = require('scripts.crystalui.widgets.padding')

local function setLabel(self, label)
    self.content[3].props.text = label
end

local function Icon(options)
    return {
        _setLabel = setLabel,
        type = ui.TYPE.Flex,
        name = options.name or nil,
        userData = {},
        props = {
            horizontal = true,
            relativePosition = options.relativePosition or vec2(0, 0),
        },
        external = {
            grow = options.grow or 0,
            stretch = options.stretch or 0,
        },
        content = ui.content {
            {
                type = ui.TYPE.Image,
                props = {
                    resource = ui.texture { path = options.icon },
                    size = vec2(16, 16),
                },
            },
            Padding(options.padding or 0),
            {
                template = I.MWUI.templates.textNormal,
                props = {
                    textAlignH = ui.ALIGNMENT.Start,
                    textAlignV = ui.ALIGNMENT.Center,
                    text = options.label or '',
                    readOnly = true,
                    textSize = 16,
                    inheritAlpha = false,
                    autoSize = true,
                },
            },
        }
    }
end

return Icon
