local ui = require('openmw.ui')
local util = require('openmw.util')
local vec2 = require('openmw.util').vector2

local I = require('openmw.interfaces')

local Template = {
    type = ui.TYPE.Container,
    content = ui.content {
        {
            props = {
                relativeSize = vec2(1, 1),
                size = vec2(4 * 2, 4 * 2),
            },
            content = ui.content {
                {
                    type = ui.TYPE.Image,
                    props = {
                        relativeSize = vec2(0, 1),
                        resource = ui.texture { path = "textures/menu_thin_border_left.dds" },
                        size = vec2(2, 0),
                        tileV = true,
                    },
                },
                {
                    type = ui.TYPE.Image,
                    props = {
                        relativeSize = vec2(0, 1),
                        relativePosition = vec2(1, 0),
                        position = vec2(-2, 0),
                        resource = ui.texture { path = "textures/menu_thin_border_right.dds" },
                        size = vec2(2, 0),
                        tileV = true,
                    },
                },
                {
                    type = ui.TYPE.Image,
                    props = {
                        relativeSize = vec2(1, 0),
                        resource = ui.texture { path = "textures/menu_thin_border_top.dds" },
                        size = vec2(0, 2),
                        tileH = true,
                    },
                },
                {
                    type = ui.TYPE.Image,
                    props = {
                        relativeSize = vec2(1, 0),
                        relativePosition = vec2(0, 1),
                        position = vec2(0, -2),
                        resource = ui.texture { path = "textures/menu_thin_border_bottom.dds" },
                        size = vec2(0, 2),
                        tileH = true,
                    },
                },
            }
        },
        {
            external = { slot = true },
            props = {
                position = vec2(4, 4),
                relativeSize = vec2(1, 1),
            }
        }
    }
}

local function FillBar(options)
    return {
        template = Template,
        content = ui.content {
            {
                props = {
                    size = options.size or vec2(100, 1),
                    relativeSize = vec2(1, 1),
                },
                content = ui.content {
                    {
                        type = ui.TYPE.Image,
                        props = {
                            relativeSize = vec2(options.percent, 1),
                            alpha = 0.8,
                            color = options.color and options.color or util.color.hex("ffffff"),
                            resource = ui.texture { path = "white" },
                        },
                        content = ui.content {
                            {
                                type = ui.TYPE.Image,
                                props = {
                                    relativeSize = vec2(1, 1),
                                    resource = ui.texture { path = "textures/crystalui/vgradient_white.dds" },
                                    color = util.color.hex('000000'),
                                    alpha = 0.5,
                                }
                            }
                        }
                    }
                }
            }
        }
    }
end

return FillBar
