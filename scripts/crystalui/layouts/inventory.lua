local ui = require('openmw.ui')
local types = require('openmw.types')
local util = require('openmw.util')
local async = require('openmw.async')
local openmw_self = require('openmw.self')
local input = require('openmw.input')
local core = require('openmw.core')
local I = require('openmw.interfaces')

local vec2 = require('openmw.util').vector2

local Palette = require('scripts.crystalui.widgets.palette')
local widgets = require('scripts.crystalui.widgets')
local state = require("scripts.crystalui.state.state")
local Window = require("scripts.crystalui.templates.window")

local UI_FilterHeaders = nil
local UI_ItemView = nil

local function updateStore()
    if (state.Inventory.filterId == nil) then return end

    local inventory = types.Actor.inventory(openmw_self):getAll()

    local items = {}

    local filter = state.Inventory.Filters[state.Inventory.filterId].filter
    for _, item in ipairs(inventory) do
        local item = filter(item)
        if (item) then
            items[#items + 1] = item
        end
    end

    UI_ItemView[1].userData.items = items
    UI_ItemView[1]:_update()
end

local function show(self)
    if self.props.visible then return end

    self.props.visible = true
    updateStore()
end

local function hide(self)
    self.props.visible = false
end

local function updateTaskbarInfo()
    local inventory = types.Actor.inventory(openmw_self):getAll()

    for _, item in ipairs(inventory) do
        if item.recordId == 'gold_001' then
            state.gold = item.count
        end
    end
end

local function onFilterChange(widget)
    for _, v in ipairs(UI_FilterHeaders) do
        if v._toggle then
            v:_toggle(false)
        end
    end
    widget:_toggle(true)

    if (state.Inventory.filterId == widget.userData.id) then return end

    state.Inventory.filterId = widget.userData.id
    updateStore()
end

local function frame(self, el)
    if not self.props.visible then
        return
    end

    self:_updateTaskbarInfo()

    state.encumbrance = types.Actor.getEncumbrance(openmw_self.object)
    state.capacity = types.NPC.getCapacity(openmw_self.object)

    UI_Weight:_setLabel(string.format('%i / %i', state.encumbrance, state.capacity))
    UI_Gold:_setLabel(tostring(state.gold))
    UI_Armor:_setLabel(tostring(state.armor))

    el:update()
end

local function createHeader(key, checked)
    return widgets.CheckButton({
        label = state.Inventory.Filters[key].label,
        id = key,
        onChange = onFilterChange,
        checked = checked,
    })
end

UI_FilterHeaders = ui.content {
    createHeader(state.Inventory.FilterIds.All, true),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Weapon),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Apparel),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Magic),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Misc),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Lockpicks),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Key),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Potions),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Ingredients),
    widgets.Padding(12),
    createHeader(state.Inventory.FilterIds.Stash),
    widgets.Padding(12),

    {
        template = I.MWUI.templates.box,
        external = {
            grow = 1,
            stretch = 1,
        },
        content = ui.content {
            {
                template = I.MWUI.templates.textEditLine,
                props = {
                    size = vec2(200, 25),
                    relativeSize = vec2(1, 1),
                }
            },
        }
    }
}

UI_ItemView = ui.content {
    widgets.ItemView({
        type = "Inventory",
        height = state.Inventory.itemViewHeight,
        columns = {
            { label = "", width = 48, image = true, },
            {
                sortFn = function(a, b)
                    if a.name == b.name then return a.id < b.id end
                    return a.name < b.name
                end,
                label = "Name",
                width = 850,
                getText = function(item) return tostring(item.name or "undefined") end,
                getIcons = function(item)
                    local icons = {}
                    if item.equipped then icons[#icons + 1] = "textures/crystalui/icons/equipped.dds" end
                    if item.enchant then icons[#icons + 1] = "textures/crystalui/icons/enchanted.dds" end
                    if item.bound then icons[#icons + 1] = "textures/crystalui/icons/bound.dds" end
                    if item.stolen then icons[#icons + 1] = "textures/crystalui/icons/stolen.dds" end

                    return icons
                end
            },
            {
                sortFn = function(a, b)
                    if a.value == b.value then return a.id < b.id end
                    return a.value > b.value
                end,
                label = "Value",
                width = 100,
                getText = function(item) return tostring(item.value or 0) end
            },
            {
                sortFn = function(a, b)
                    local vwa
                    local vwb

                    if a.weight == 0 then
                        vwa = math.huge -- Handle division by zero by setting to infinity
                    else
                        vwa = a.value / a.weight
                    end

                    if b.weight == 0 then
                        vwb = math.huge -- Handle division by zero by setting to infinity
                    else
                        vwb = b.value / b.weight
                    end

                    if vwa == vwb then
                        return a.id < b.id
                    end

                    return vwa > vwb
                end,
                label = "V/W",
                width = 100,
                getText = function(item)
                    return item.weight == 0 and '-' or
                        string.format("%.2f", item.value / item.weight)
                end
            },
            {
                sortFn = function(a, b)
                    if a.totalWeight == b.totalWeight then return a.id < b.id end
                    return a.totalWeight > b.totalWeight
                end,
                label = "Weight",
                width = 100,
                getText = function(item) return string.format("%.2f", item.totalWeight or 0) end
            },
            {
                sortFn = function(a, b)
                    if a.count == b.count then return a.id < b.id end
                    if a.count == b.count then
                        return a.id > b.id
                    else
                        return (a.count or 1) > (b.count or 1)
                    end
                end,
                label = "Count",
                width = 100,
                getText = function(item) return tostring(item.count or 1) end
            },
        }
    })
}

UI_Gold = widgets.Icon({
    icon = 'icons/gold.dds',
    padding = 8,
    name = "UI_Gold",
    relativePosition = vec2(0.5, 0)
})
UI_Armor = widgets.Icon({ icon = 'icons/a/tx_steel_curaiss.dds', padding = 8, name = "UI_Armor" })
UI_Weight = widgets.Icon({ icon = 'icons/weight.dds', padding = 8, name = "UI_Weight" })

local layout = {
    _show = show,
    _hide = hide,
    _updateStore = updateStore,
    _updateTaskbarInfo = updateTaskbarInfo,
    _frame = frame,
    layer = 'Windows',
    template = Window,
    props = {
        relativePosition = vec2(0.0, 0.5),
        anchor = vec2(0.0, 0.5),
        visible = true,
        position = vec2(150, 20),
    },
    events = {
        keyPress = async:callback(function(e, data)
            if e.code == input.KEY.DownArrow then
                -- UI_ItemView[1]:_selectNextRow()
            elseif e.code == input.KEY.UpArrow then
                -- UI_ItemView[1]:_selectPreviousRow()
            elseif e.code == input.KEY.E then
                local selected = state.Inventory.hoverItem or state.Inventory.selectedItem
                if not selected then return true end

                core.sendGlobalEvent('UseItem', { object = selected.ref, actor = openmw_self.object, force = true })
            end
        end),
    },
    content = ui.content {
        {
            type = ui.TYPE.Flex,
            props = {
                horizontal = false,
            },
            content = ui.content {
                {
                    type = ui.TYPE.Flex,
                    props = {
                        horizontal = true,
                    },
                    external = {
                        stretch = 1,
                    },
                    content = ui.content {
                        {
                            type = ui.TYPE.Flex,
                            props = {
                                relativeSize = vec2(1, 0),
                                horizontal = true,
                            },
                            external = {
                                grow = 1,
                            },
                            content = UI_FilterHeaders,
                        },
                    },
                },
                widgets.Padding(12),
                {
                    external = {
                        grow = 1,
                        stretch = 1,
                    },
                    props = {
                        relativeSize = vec2(1, 0),
                        size = vec2(1, state.Inventory.itemViewHeight)
                    },
                    content = UI_ItemView,
                },
                widgets.Padding(12),
                {
                    type = ui.TYPE.Image,
                    external = {
                        stretch = 1,
                    },
                    props = {
                        relativeSize = vec2(1, 0),
                        size = vec2(0, 2),
                        resource = ui.texture { path = "white" },
                        color = Palette.Text_Normal,
                        alpha = 0.2,
                    }
                },
                widgets.Padding(24),
                {
                    type = ui.TYPE.Widget,
                    external = {
                        stretch = 1,
                        grow = 1,
                    },
                    props = {
                        size = vec2(0, 16),
                        relativeSize = vec2(1, 1),
                    },
                    content = ui.content {
                        -- Gold icon and label
                        UI_Gold,
                        {
                            type = ui.TYPE.Flex,
                            props = {
                                relativeSize = vec2(1, 0),
                                horizontal = true,
                            },
                            external = {
                                grow = 1,
                                stretch = 1,
                            },
                            content = ui.content {
                                -- Left Padding
                                widgets.Padding(8),

                                -- -- Armor icon and label
                                UI_Armor,
                                widgets.Spacer(),

                                -- -- Encumbrance icon and label
                                UI_Weight,

                                -- -- Right Padding
                                widgets.Padding(8),
                            },
                        },
                    }
                },
            }
        }
    },
}

return layout
