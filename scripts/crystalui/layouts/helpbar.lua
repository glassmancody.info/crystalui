local ui = require('openmw.ui')
local util = require('openmw.util')
local I = require('openmw.interfaces')
local vec2 = require('openmw.util').vector2

local Palette = require('scripts.crystalui.widgets.palette')
local widgets = require('scripts.crystalui.widgets')
local state = require("scripts.crystalui.state.state")

local function show(self)
    self.props.visible = true
end

local function hide(self)
    self.props.visible = false
    self.userData.dirty = true
end

local function frame(self, el)
    if not self.props.visible then
        if self.userData.dirty == true then
            self.userData.dirty = false
            el:update()
        end
        return
    end

    el:update()
end

local function helpButton(options)
    return {
        template = I.MWUI.templates.box,
        props = {
            relativeSize = vec2(1, 1),
        },
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                content = ui.content {
                    {
                        template = I.MWUI.templates.textNormal,
                        props = {
                            textAlignH = ui.ALIGNMENT.Center,
                            textAlignV = ui.ALIGNMENT.Center,
                            text = options.text,
                            autoSize = false,
                            size = vec2(28, 28),
                            textSize = 28,
                        },
                    },
                },
            }
        },
    }
end

local function helpText(options)
    return {
        template = I.MWUI.templates.textNormal,
        props = {
            textAlignH = ui.ALIGNMENT.Center,
            textAlignV = ui.ALIGNMENT.Center,
            text = options.text,
            textSize = 28,
        },
    }
end

local layout = {
    _show = show,
    _hide = hide,
    _frame = frame,
    layer = 'HUD',
    userData = {
        dirty = true,
    },
    props = {
        relativePosition = vec2(0.5, 1.0),
        anchor = vec2(0.5, 1.0),
        visible = true,
        position = vec2(20, 10),
        size = vec2(-80, 80),
        relativeSize = vec2(1.0, 0.0),
    },
    content = ui.content {
        {
            type = ui.TYPE.Flex,
            props = {
                arrange = ui.ALIGNMENT.Center,
                horizontal = true,
            },
            content = ui.content {
                helpButton({ text = 'E' }),
                widgets.Padding(16),
                helpText({ text = 'Equip' }),

                widgets.Padding(32),
                helpButton({ text = 'R' }),
                widgets.Padding(16),
                helpText({ text = 'Drop' }),

                widgets.Padding(32),
                helpButton({ text = 'F' }),
                widgets.Padding(16),
                helpText({ text = 'Favorite' }),

                widgets.Padding(32),
                helpButton({ text = 'S' }),
                widgets.Padding(16),
                helpText({ text = 'Stash' }),
            }
        }
    },
}

return layout
