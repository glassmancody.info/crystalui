local ui = require('openmw.ui')
local util = require('openmw.util')
local vec2 = require('openmw.util').vector2

local Palette = require('scripts.crystalui.widgets.palette')
local widgets = require('scripts.crystalui.widgets')
local state = require("scripts.crystalui.state.state")

local Tooltips = require('scripts.crystalui.tooltips.interface')

local lastRendered = nil
local dirty = false

local function show(self)
    self.props.visible = true
    dirty = true
end

local function hide(self)
    self.props.visible = false
    dirty = true
end

local function frame(self, el)
    if not self.props.visible then
        el:update()
        return
    end

    if not state.Inventory.selectedItem then
        self.props.visible = false
        el:update()
        return
    end

    if state.Inventory.selectedItem == lastRendered then
        if dirty then
            el:update()
            dirty = false
        end
        return
    end

    lastRendered = state.Inventory.selectedItem
    self.template.content[1].content[2].props.resource = ui.texture { path = lastRendered.icon }
    self.content[1].content[2].props.text = lastRendered.name
    self.content[1].content[5].props.text = Tooltips.getTooltip(lastRendered.recordId)

    el:update()
end

local RootTemplate = {
    type = ui.TYPE.Container,
    content = ui.content {
        {
            props = {
                relativeSize = vec2(1, 1),
                size = vec2(24, 24 * 2),
            },
            content = ui.content {
                {
                    type = ui.TYPE.Image,
                    props = {
                        relativeSize = vec2(1, 1),
                        resource = ui.texture { path = "white" },
                        color = util.color.hex("000000"),
                        alpha = 0.5,
                        position = vec2(0, 24),
                        size = vec2(0, -24),
                        anchor = vec2(0.0, 0.0),
                    },
                },

                {
                    type = ui.TYPE.Image,
                    props = {
                        resource = ui.texture { path = "transparent" },
                        alpha = 1,
                        size = vec2(48, 48),
                        relativePosition = vec2(0.5, 0.0),
                        position = vec2(0, 24),
                        anchor = vec2(0.5, 0.5),
                    },
                },
            }
        },
        {
            external = { slot = true },
            props = {
                position = vec2(12, 34),
                relativeSize = vec2(1, 1),
            }
        },
    },
}

local layout = {
    _show = show,
    _hide = hide,
    _frame = frame,
    layer = 'Windows',
    template = RootTemplate,
    props = {
        relativePosition = vec2(0.5, 1.0),
        anchor = vec2(0.0, 1.0),
        visible = true,
        position = vec2(400, -200),
    },
    content = ui.content {
        {
            type = ui.TYPE.Flex,
            props = {
                horizontal = false,
            },
            content = ui.content {
                {
                    type = ui.TYPE.Text,
                    props = {
                        size = vec2(600, 25),
                        text = "",
                        autoSize = false,
                        textSize = 20,
                        textColor = util.color.hex('fffffd'),
                        textAlignH = ui.ALIGNMENT.Start,
                        textAlignV = ui.ALIGNMENT.Center,
                    }
                },
                {
                    type = ui.TYPE.Text,
                    props = {
                        size = vec2(600, 50),
                        text = "",
                        autoSize = false,
                        textSize = 32,
                        textColor = util.color.hex('fffffd'),
                        textAlignH = ui.ALIGNMENT.Center,
                        textAlignV = ui.ALIGNMENT.Center,
                    }
                },
                {
                    type = ui.TYPE.Image,
                    props = {
                        relativeSize = vec2(1, 0),
                        size = vec2(600, 2),
                        resource = ui.texture { path = "white" },
                        color = Palette.Text_Normal,
                        alpha = 0.2,
                    }
                },
                {
                    props = {
                        size = vec2(0, 24),
                    }
                },
                {
                    type = ui.TYPE.Text,
                    props = {
                        size = vec2(600, 80),
                        text = "",
                        autoSize = false,
                        multiline = true,
                        wordWrap = true,
                        readOnly = true,
                        textSize = 16,
                        textColor = Palette.Text_Normal,
                        textAlignH = ui.ALIGNMENT.Center,
                        textAlignV = ui.ALIGNMENT.Start,
                    }
                },
            }
        }
    },
}

return layout
