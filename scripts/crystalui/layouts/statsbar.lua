local ui = require('openmw.ui')
local util = require('openmw.util')
local types = require('openmw.types')
local vec2 = require('openmw.util').vector2
local openmw_self = require('openmw.self')
local I = require('openmw.interfaces')

local Palette = require('scripts.crystalui.widgets.palette')
local widgets = require('scripts.crystalui.widgets')
local state = require("scripts.crystalui.state.state")

local LevelStats = require('openmw.types').Actor.stats.level
local DynamicStats = require('openmw.types').Actor.stats.dynamic

local function show(self)
    self.props.visible = true
end

local function hide(self)
    self.props.visible = false
    self.userData.dirty = true
end

local function frame(self, el)
    if not self.props.visible then
        if self.userData.dirty == true then
            self.userData.dirty = false
            el:update()
        end
        return
    end

    el:update()
end

local function getEquippedItemLeft()
    local equipped = openmw_self.object.type.getEquipment(openmw_self.object, types.Actor.EQUIPMENT_SLOT.CarriedLeft)

    if equipped then return ui.texture { path = equipped.type.record(equipped).icon } end

    return nil
end

local function getEquippedItemRight()
    local equipped = openmw_self.object.type.getEquipment(openmw_self.object, types.Actor.EQUIPMENT_SLOT.CarriedRight)

    if equipped then return ui.texture { path = equipped.type.record(equipped).icon } end

    return nil
end

local function getEquippedSpell()
    local spell = openmw_self.object.type.getSelectedSpell(openmw_self.object)
    if spell then
        local path = spell.effects[1].effect.icon
        local lastSlashIndex = path:match(".*\\()")
        local modifiedPath = path:sub(1, lastSlashIndex - 1) .. 'b_' .. path:sub(lastSlashIndex)

        return ui.texture { path = modifiedPath }
    end

    return nil
end

local function iconWithFillbar(options)
    return {
        type = ui.TYPE.Flex,
        external = {
            stretch = 1,
        },
        content = ui.content {
            {
                template = I.MWUI.templates.box,
                content = ui.content {
                    {
                        props = {
                            size = vec2(54, 50),
                        },
                        content = ui.content {
                            {
                                type = ui.TYPE.Image,
                                props = {
                                    alpha = 1,
                                    resource = options.texture,
                                    size = vec2(50, 50),
                                    relativePosition = vec2(0.5, 0.5),
                                    anchor = vec2(0.5, 0.5),
                                },
                            }
                        }
                    }
                }
            },
            widgets.Padding(6),
            widgets.FillBar({ size = vec2(50, 2), color = options.color, percent = options.percent }),
        }
    }
end

local function barWithSideText(options)
    return {
        type = ui.TYPE.Flex,
        props = {
            horizontal = true,
            arrange    = ui.ALIGNMENT.Center,
        },
        content = ui.content {
            widgets.FillBar({ size = vec2(225, 4), color = options.color, percent = options.percent }),
            widgets.Padding(12),
            {
                type = ui.TYPE.Text,
                props = {
                    text = options.text,
                    textSize = 16,
                    textColor = util.color.hex('A6A6A6'),
                }
            },
        }
    }
end

local layout = {
    _show = show,
    _hide = hide,
    _frame = frame,
    layer = 'HUD',
    userData = {
        dirty = true,
    },
    props = {
        relativePosition = vec2(0.5, 0.0),
        anchor = vec2(0.5, 0.0),
        visible = true,
        position = vec2(0, 20),
        size = vec2(-100, 100),
        relativeSize = vec2(1.0, 0.0),
    },
    content = ui.content {
        {
            type = ui.TYPE.Flex,
            props = {
                horizontal = true,
                relativeSize = vec2(1, 1),
                arrange = ui.ALIGNMENT.Center,
                align = ui.ALIGNMENT.Start,
            },
            content = ui.content {
                {
                    type = ui.TYPE.Text,
                    props = {
                        text = tostring(LevelStats(openmw_self).current),
                        textSize = 80,
                        textColor = util.color.hex('fffffd'),
                        autoSize = false,
                        size = vec2(100, 100),
                        textShadow = true,
                        textShadowColor = util.color.hex('a0a0a0'),
                    }
                },
                {
                    type = ui.TYPE.Flex,
                    external = {
                        stretch = 1,
                    },
                    content = ui.content {
                        {
                            type = ui.TYPE.Text,
                            props = {
                                text = openmw_self.object.type.record(openmw_self.object).name,
                                textSize = 36,
                                textColor = util.color.hex('fffffd'),
                                textShadow = true,
                                textShadowColor = util.color.hex('a0a0a0'),
                            }
                        },
                        widgets.Padding(8),
                        widgets.FillBar({ size = vec2(340, 12), color = util.color.hex('912d44'), percent = 0.75 }),
                    }
                },
                widgets.Spacer(),
                iconWithFillbar({ color = util.color.hex('8b371a'), texture = getEquippedItemRight(), percent = 0.90 }),
                widgets.Padding(12),
                iconWithFillbar({ color = util.color.hex('8b371a'), texture = getEquippedItemLeft(), percent = 0.40 }),
                widgets.Padding(12),
                iconWithFillbar({ color = util.color.hex('396ea8'), texture = getEquippedSpell(), percent = 0.75 }),
                widgets.Padding(48),
                {
                    type = ui.TYPE.Flex,
                    external = {
                        stretch = 1,
                    },
                    props = {
                        align = ui.ALIGNMENT.Start,
                    },
                    content = ui.content {
                        barWithSideText({
                            color = util.color.hex('96391a'),
                            percent = DynamicStats.health(openmw_self.object).current /
                                DynamicStats.health(openmw_self.object).base,
                            text = string.format('%i / %i', DynamicStats.health(openmw_self.object).current,
                                DynamicStats.health(openmw_self.object).base)
                        }),
                        widgets.Padding(8),
                        barWithSideText({
                            color = util.color.hex('4986b7'),
                            percent = DynamicStats.magicka(openmw_self.object).current /
                                DynamicStats.magicka(openmw_self.object).base,
                            text = string.format('%i / %i', DynamicStats.magicka(openmw_self.object).current,
                                DynamicStats.magicka(openmw_self.object).base)
                        }),
                        widgets.Padding(8),
                        barWithSideText({
                            color = util.color.hex('8bb141'),
                            percent = DynamicStats.fatigue(openmw_self.object).current /
                                DynamicStats.fatigue(openmw_self.object).base,
                            text = string.format('%i / %i', DynamicStats.fatigue(openmw_self.object).current,
                                DynamicStats.fatigue(openmw_self.object).base)
                        }),
                        widgets.Padding(8),
                    }
                },
            }
        }
    },
}

return layout
