local ui = require('openmw.ui')
local types = require('openmw.types')
local util = require('openmw.util')
local openmw_self = require('openmw.self')
local vec2 = require('openmw.util').vector2
local I = require('openmw.interfaces')
local core = require('openmw.core')

local Palette = require('scripts.crystalui.widgets.palette')
local widgets = require('scripts.crystalui.widgets')
local state = require("scripts.crystalui.state.state")

local Window = require("scripts.crystalui.templates.window")

local UI_FilterHeaders = nil
local UI_ItemView = nil

local function show(self)
    self.props.visible = true
end

local function hide(self)
    self.props.visible = false
end

local function updateStore(self)
    if (state.Magic.filterId == nil) then return end

    local spells = types.Actor.spells(openmw_self)

    local items = {}

    local filter = state.Magic.Filters[state.Magic.filterId].filter
    for _, item in ipairs(spells) do
        local spell = filter(item)
        if (spell) then
            items[#items + 1] = spell
        end
    end

    UI_ItemView[1].userData.items = items
    UI_ItemView[1]:_update()
end

local function onFilterChange(self)
    for _, v in ipairs(UI_FilterHeaders) do
        if v._toggle and v.userData.id ~= self.userData.id then
            v:_toggle(false)
        end
    end

    self:_toggle(true)

    if (state.Magic.filterId == self.userData.id) then return end

    state.Magic.filterId = self.userData.id
    updateStore()
end

local function frame(self)
end

local function createHeader(key, checked)
    return widgets.CheckButton({
        label = state.Magic.Filters[key].label,
        id = key,
        onChange = onFilterChange,
        checked = checked,
    })
end

UI_FilterHeaders = ui.content {
    createHeader(state.Magic.FilterIds.All, true),
    widgets.Padding(12),
    createHeader(state.Magic.FilterIds.Powers),
    widgets.Padding(12),
    createHeader(state.Magic.FilterIds.Illusion),
    widgets.Padding(12),
    createHeader(state.Magic.FilterIds.Conjuration),
    widgets.Padding(12),
    createHeader(state.Magic.FilterIds.Alteration),
    widgets.Padding(12),
    createHeader(state.Magic.FilterIds.Destruction),
    widgets.Padding(12),
    createHeader(state.Magic.FilterIds.Mysticism),
    widgets.Padding(12),
    createHeader(state.Magic.FilterIds.Restoration),
    widgets.Padding(12),
}

UI_ItemView = ui.content {
    widgets.ItemView({
        type = "Magic",
        height = state.Magic.itemViewHeight,
        columns = {
            { label = "", width = 48, image = true, },
            {
                sortFn = function(a, b)
                    if a.name == b.name then return a.id < b.id end
                    return a.name < b.name
                end,
                label = "Name",
                width = 550,
                getText = function(spell) return tostring(spell.name or "undefined") end
            },
            {
                sortFn = function(a, b)
                    if a.school == b.school then return a.id < b.id end
                    return a.school > b.school
                end,
                label = "School",
                width = 100,
                getText = function(spell) return tostring(spell.school or 0) end
            },
            {
                sortFn = function(a, b)
                    local cca
                    local ccb

                    if a.chance == 0 then
                        cca = math.huge -- Handle division by zero by setting to infinity
                    else
                        cca = a.cost / a.chance
                    end

                    if b.chance == 0 then
                        ccb = math.huge -- Handle division by zero by setting to infinity
                    else
                        ccb = b.cost / b.chance
                    end

                    if cca == ccb then
                        return a.id < b.id
                    end

                    return cca > ccb
                end,
                label = "Cost/Chance",
                width = 100,
                getText = function(spell)
                    if spell.type ~= core.magic.SPELL_TYPE.Spell then return '-' end

                    return spell.chance == 0 and '-' or
                        string.format("%.2f", spell.cost / spell.chance)
                end
            },
        }
    })
}

local layout = {
    _show = show,
    _hide = hide,
    _updateStore = updateStore,
    _frame = frame,
    layer = 'Windows',
    template = Window,
    props = {
        relativePosition = vec2(0.8, 0.5),
        anchor = vec2(0.5, 0.5),
        size = vec2(500, 500),
        visible = true,
    },
    content = ui.content {
        {
            type = ui.TYPE.Flex,
            props = {
                horizontal = false,
            },
            content = ui.content {
                {
                    type = ui.TYPE.Flex,
                    props = {
                        horizontal = true,
                    },
                    external = {
                        stretch = 1,
                    },
                    content = ui.content {
                        {
                            type = ui.TYPE.Flex,
                            props = {
                                relativeSize = vec2(1, 0),
                                horizontal = true,
                            },
                            external = {
                                grow = 1,
                            },
                            content = UI_FilterHeaders,
                        },
                    },
                },
                widgets.Padding(12),
                {
                    external = {
                        grow = 1,
                        stretch = 1,
                    },
                    props = {
                        relativeSize = vec2(1, 0),
                        size = vec2(1, state.Magic.itemViewHeight)
                    },
                    content = UI_ItemView,
                },
            }
        }
    },
}

return layout
