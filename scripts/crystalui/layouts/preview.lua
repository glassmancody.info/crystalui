local ui = require('openmw.ui')
local util = require('openmw.util')
local vec2 = require('openmw.util').vector2

local Palette = require('scripts.crystalui.widgets.palette')
local widgets = require('scripts.crystalui.widgets')
local state = require("scripts.crystalui.state.state")

local scenegraph = require('openmw.scenegraph')


local lastRendered = nil

local function show(self)
    self.props.visible = true
end

local function hide(self)
    self.props.visible = false
    self.userData.dirty = true
end

local function frame(self, el)
    if not self.props.visible then
        return
    end

    if not state.Inventory.selectedItem then
        self.props.visible = false
        el:update()
        return
    end

    -- if state.Inventory.selectedItem == lastRendered then return end

    if lastRendered and lastRendered.model == state.Inventory.selectedItem.model then
        if self.userData.dirty then
            self.userData.dirty = false
            el:update()
        else
            return
        end
    end

    lastRendered = state.Inventory.selectedItem

    local rttCamera = scenegraph.camera {
        fov = 75,
        near = 1,
        far = 1999,
        resolution = util.vector2(1024, 1024),
        clearColor = util.color.rgba(0, 0, 0, 0),
        ambientColor = util.color.rgba(10, 10, 11, 1),
        viewMatrix = util.transform.lookAt(util.vector3(-50, 0, 0), util.vector3(0, 0, 0), util.vector3(0, 1, 0)),
    }
    rttCamera.rootNode.children[1] = scenegraph.group { attitude = util.transform.rotateY(90), position = util.vector3(0,
        -5, 0) }
    rttCamera.rootNode.children[1].scale = util.vector3(0.7, 0.7, 0.7)
    rttCamera.rootNode.children[1].children[1] = scenegraph.mesh { path = lastRendered.model }
    self.content[1].props.resource = ui.texture { texture = rttCamera.texture }

    -- self.content[1].props.resource = ui.texture { path = lastRendered.icon }
    -- print(lastRendered.ref:getBoundingBox())

    el:update()
end

local layout = {
    _show = show,
    _hide = hide,
    _frame = frame,
    layer = 'HUD',
    userData = {
        dirty = true,
    },
    props = {
        relativePosition = vec2(0.5, 1.0),
        anchor = vec2(0.0, 1.0),
        visible = true,
        position = vec2(200, -200),
        size = vec2(1000, 1100),
    },
    content = ui.content {
        {
            type = ui.TYPE.Image,
            props = {
                relativeSize = vec2(1, 1),
                -- resource = ui.texture { path = "white" },
            }
        }
    },
}

return layout
