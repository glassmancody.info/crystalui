local ui = require('openmw.ui')
local util = require('openmw.util')
local I = require('openmw.interfaces')
local postprocessing = require('openmw.postprocessing')

local ShaderHud = postprocessing.load('CrystalUI')

local Inventory_Element = ui.create(require("scripts.crystalui.layouts.inventory"))
-- local Magic_Element = ui.create(require("scripts.crystalui.layouts.magic"))
local DockTooltip_Element = ui.create(require("scripts.crystalui.layouts.dockedtooltip"))
local Preview_Element = ui.create(require("scripts.crystalui.layouts.preview"))
local HelpBar_Element = ui.create(require("scripts.crystalui.layouts.helpbar"))
local StatsBar_Element = ui.create(require("scripts.crystalui.layouts.statsbar"))

local function onActive()
    Inventory_Element.layout:_updateStore()
    -- Magic_Element.layout:_updateStore()

    I.UI.registerWindow("Inventory",
        function()
            Inventory_Element.layout:_show()
            DockTooltip_Element.layout:_show()
            Preview_Element.layout:_show()
            HelpBar_Element.layout:_show()
            StatsBar_Element.layout:_show()

            I.UI.setHudVisibility(false)

            ShaderHud:enable()
        end,
        function()
            Inventory_Element.layout:_hide()
            DockTooltip_Element.layout:_hide()
            Preview_Element.layout:_hide()
            HelpBar_Element.layout:_hide()
            StatsBar_Element.layout:_hide()

            Inventory_Element:update()
            DockTooltip_Element:update()
            Preview_Element:update()

            I.UI.setHudVisibility(true)

            ShaderHud:disable()
        end
    )
    -- register("Magic", Magic_Element)
    I.UI.registerWindow("Magic", function() end, function() end)

    I.UI.registerWindow("Map", function() end, function() end)
    I.UI.registerWindow("Stats", function() end, function() end)

    I.UI.setPauseOnMode('Interface', false)
end

local lastResolution = nil

local function onFrame()
    Inventory_Element.layout:_frame(Inventory_Element)
    DockTooltip_Element.layout:_frame(DockTooltip_Element)
    Preview_Element.layout:_frame(Preview_Element)
    HelpBar_Element.layout:_frame(HelpBar_Element)
    StatsBar_Element.layout:_frame(StatsBar_Element)
    -- Magic_Element.layout:_frame()

    if lastResolution ~= ui.screenSize() then
        lastResolution = ui.screenSize()
        ShaderHud:setVector2("uBlurBounds", util.vector2(150, ui.screenSize().y - 50))
    end
end

return {
    engineHandlers = {
        onActive = onActive,
        onFrame = onFrame,
    },
    eventHandlers = {
        crystalui_UpdateInventory = function()
            Inventory_Element.layout:_updateStore(Inventory_Element)
        end
    }
}
