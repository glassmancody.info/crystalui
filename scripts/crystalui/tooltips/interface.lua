local data = require('scripts.crystalui.tooltips.data')

local this = {
    getTooltip = function(id)
        for _, group in pairs(data) do
            local description = group[id]
            if description ~= nil then return description end
        end
        return ''
    end,
}

return this
