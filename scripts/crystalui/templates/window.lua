local ui = require('openmw.ui')
local util = require('openmw.util')
local vec2 = require('openmw.util').vector2

local Window = {
    type = ui.TYPE.Container,
    content = ui.content {
        {
            type = ui.TYPE.Image,
            props = {
                relativeSize = vec2(1, 1),
                resource = ui.texture { path = "white" },
                color = util.color.hex("000000"),
                alpha = 0.5,
                size = vec2(24 * 2, 24 * 2),
            },
        },
        {
            external = { slot = true },
            props = {
                position = vec2(24, 24),
                relativeSize = vec2(1, 1),
            }
        },
    },
}

return Window
