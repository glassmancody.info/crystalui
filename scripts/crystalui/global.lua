local function UseItem(data)
    if data.actor then
        data.actor:sendEvent('crystalui_UpdateInventory')
    end
end

return {
    eventHandlers = { UseItem = UseItem },
}
