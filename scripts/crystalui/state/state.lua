local ui = require('openmw.ui')

local State = {
    gold = 0,
    armor = 0,
    encumbrance = 0,
    capacity = 0,
    Inventory = {
        Filters = require('scripts.crystalui.state.filters.items'),
        FilterIds = require('scripts.crystalui.state.filters.ids').Inventory,
        filterId = 0,
        itemViewHeight = ui.screenSize().y - 400,
        itemViewScrollPos = 0,
        selectedRow = nil,
        selectedItem = nil,
        hoverItem = nil,
        selectedSorterDir = true,
        selectedSorter = 'Name',
    },
    Magic = {
        Filters = require('scripts.crystalui.state.filters.magic'),
        FilterIds = require('scripts.crystalui.state.filters.ids').Magic,
        filterId = 0,
        itemViewHeight = ui.screenSize().y - 400,
        itemViewScrollPos = 0,
        selectedRow = nil,
        selectedItem = nil,
        hoverItem = nil,
        selectedSorterDir = true,
        selectedSorter = 'Name',
    },
}

return State
