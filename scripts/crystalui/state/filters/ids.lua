return {
    Inventory = {
        All = 0,
        Weapon = 1,
        Apparel = 2,
        Magic = 3,
        Misc = 4,
        Lockpicks = 5,
        Key = 6,
        Potions = 7,
        Ingredients = 8,
        Stash = 9,
    },
    Magic = {
        All = 0,
        Powers = 1,
        Illusion = 2,
        Conjuration = 3,
        Alteration = 4,
        Destruction = 5,
        Mysticism = 6,
        Restoration = 7,
    }
}
