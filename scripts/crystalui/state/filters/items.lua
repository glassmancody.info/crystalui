local ui = require('openmw.ui')
local types = require('openmw.types')
local core = require('openmw.core')
local openmw_self = require('openmw.self')

local FilterIds = require('scripts.crystalui.state.filters.ids').Inventory

local boundIDs = require('scripts.crystalui.bound')

local function create(item)
    local record = item.type.record(item)
    return {
        enchant = (record.enchant ~= nil and record.enchant ~= '') and record.enchant or nil,
        recordId = item.recordId,
        id = item.id,
        name = record.name,
        weight = record.weight,
        totalWeight = record.weight * item.count,
        value = record.value,
        icon = record.icon,
        count = item.count,
        type = record.type,
        ref = item,
        model = record.model,
        equipped = types.Actor.hasEquipped(openmw_self, item),
        bound = boundIDs[item.recordId],
        stolen = false, -- this is too complicated
    }
end

return {
    [FilterIds.All] = {
        label = 'All',
        filter = function(item)
            if item.stashed then return end
            return create(item)
        end,
    },
    [FilterIds.Weapon] = {
        label = 'Weapons',
        filter = function(item)
            if item.stashed or item.type ~= types.Weapon then return nil end
            return create(item)
        end,
    },
    [FilterIds.Apparel] = {
        label = 'Apparel',
        filter = function(item)
            if item.stashed or item.type ~= types.Clothing then return nil end
            return create(item)
        end,
    },
    [FilterIds.Magic] = {
        label = 'Magic',
        filter = function(item)
            if item.stashed or item.type ~= types.Book then return nil end
            return create(item)
        end,
    },
    [FilterIds.Misc] = {
        label = 'Misc',
        filter = function(item)
            if item.stashed or item.type ~= types.Miscellaneous then return nil end
            return create(item)
        end,
    },
    [FilterIds.Lockpicks] = {
        label = 'Lockpicks',
        filter = function(item)
            if item.stashed or item.type ~= types.Lockpick and item.type ~= types.Probe then return nil end
            return create(item)
        end,
    },
    [FilterIds.Key] = {
        label = 'Keys',
        filter = function(item)
            if item.stashed or item.type ~= types.Miscellaneous then return nil end
            return create(item)
        end,
    },
    [FilterIds.Potions] = {
        label = 'Potions',
        filter = function(item)
            if item.stashed or item.type ~= types.Potion then return nil end
            return create(item)
        end,
    },
    [FilterIds.Ingredients] = {
        label = 'Ingredients',
        filter = function(item)
            if item.stashed or item.type ~= types.Ingredient then return nil end
            return create(item)
        end,
    },
    [FilterIds.Stash] = {
        label = 'Stash',
        filter = function(item)
            if not item.stashed then return nil end
            return create(item)
        end,
    },
}
