local types = require('openmw.types')
local core = require('openmw.core')

local FilterIds = require('scripts.crystalui.state.filters.ids').Magic

-- Missing from API? This is annoying to calculate and I'm not sure if it's even possible
local function getSpellSuccessChance(spell)
    if spell.type == core.magic.SPELL_TYPE.Power then return 100 end
    if spell.type ~= core.magic.SPELL_TYPE.Spell then return 100 end

    return 69
end

local function create(spell)
    return {
        id = spell.id,
        name = spell.name,
        icon = spell.effects[1].effect.icon,
        school = spell.effects[1].effect.school,
        cost = spell.cost,
        chance = getSpellSuccessChance(spell),
        type = spell.type,
    }
end

return {
    [FilterIds.All] = {
        label = 'All',
        filter = function(spell)
            return create(spell)
        end,
    },
    [FilterIds.Powers] = {
        label = 'Powers',
        filter = function(spell)
            if spell.type ~= core.magic.SPELL_TYPE.Power then return nil end
            return create(spell)
        end,
    },
    [FilterIds.Illusion] = {
        label = 'Illusion',
        filter = function(spell)
            if spell.effects[1].effect.school ~= 'illusion' then return end
            return create(spell)
        end,
    },
    [FilterIds.Conjuration] = {
        label = 'Conjuration',
        filter = function(spell)
            if spell.effects[1].effect.school ~= 'conjuration' then return end
            return create(spell)
        end,
    },
    [FilterIds.Alteration] = {
        label = 'Alteration',
        filter = function(spell)
            if spell.effects[1].effect.school ~= 'alteration' then return end
            return create(spell)
        end,
    },
    [FilterIds.Destruction] = {
        label = 'Destruction',
        filter = function(spell)
            if spell.effects[1].effect.school ~= 'destruction' then return end
            return create(spell)
        end,
    },
    [FilterIds.Mysticism] = {
        label = 'Mysticism',
        filter = function(spell)
            if spell.effects[1].effect.school ~= 'mysticism' then return end
            return create(spell)
        end,
    },
    [FilterIds.Restoration] = {
        label = 'Restoration',
        filter = function(spell)
            if spell.effects[1].effect.school ~= 'restoration' then return end
            return create(spell)
        end,
    },
}
